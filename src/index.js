import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import { Route, BrowserRouter as Router } from "react-router-dom";

import 'semantic-ui-css/semantic.min.css'

// Custom Components
import Home from "./components/Home";
import Gallery from "./components/Gallery";
import DetailView from "./components/DetailView";

const routing = (
  <Router basename="/mp2" >
    <div>
      <Route exact path="/" component={Home} />
      <Route path="/gallery" component={Gallery} />
      <Route path="/detail/:id" component={DetailView} />
    </div>
  </Router>
);

ReactDOM.render(routing, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
