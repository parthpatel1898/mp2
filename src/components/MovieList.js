import React from "react";
import axios from 'axios';
import { Redirect } from 'react-router';


export default class MovieList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      movies: [],
      open: false,
      selMovie: -1,
      redirect: false
    }
  }
  componentDidMount() {
    this.fetchMovie("a", "vote_count", "desc");
  }

  compareValues(key, order = 'asc') {
    return function innerSort(a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // property doesn't exist on either object
        return 0;
      }
  
      const varA = (typeof a[key] === 'string')
        ? a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string')
        ? b[key].toUpperCase() : b[key];
  
      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return (
        (order === 'desc') ? (comparison * -1) : comparison
      );
    };
  }

  fetchMovie(query, category, sort_by) {
    let base_link = "https://api.themoviedb.org/3/search/movie";
    let api_key = "1f101fb2fa544e8ca58a33576f7f3ce0";
    let language = "en-US";
    let page = "1";
    let include_adult = "false";
    let final_link = base_link + "?api_key=" + api_key + "&language=" + language + "&page=" + page
                    + "&include_adult=" + include_adult + "&query=" + query;

    axios.get(final_link).then(res => {
      const movie_data = res.data;
      this.setState({ movies: movie_data.results.sort(this.compareValues(category, sort_by)) });
    });
  }

  handleOpen(value) {
    this.setState({selMovie: value});
    this.setState({redirect: true});
  }

  render() {
    return (
      <div>
        {this.state.redirect ? <Redirect push to={"/detail/" + this.state.selMovie} /> : <Redirect push to="/" />}
        <table>
          <tbody>
            {this.state.movies.map(movie => {
              return (
                <tr key={movie.id} className={"listItem"} to={"#id=" + movie.id} onClick={() => this.handleOpen(movie.id)}>
                  <td className={"imgPoster"}>
                    <img 
                      alt={movie.title ? movie.title : movie.original_name}
                      src={
                        movie.poster_path
                          ? "http://image.tmdb.org/t/p/w185/" +
                            movie.poster_path
                          : "https://thefilmuniverse.com/wp-content/uploads/2019/09/Poster_Not_Available2.jpg"
                      }
                    />
                  </td>
                  <td className={"movieMeta"}>
                    <ul className={"movieData"}>
                      <li className={"movieTitle"}>
                        {movie.title ? movie.title : movie.original_name}
                      </li>
                      <li>Rating: {movie.vote_average} / 10</li>
                      <li>Vote Count: {movie.vote_count}</li>
                    </ul>
                    <p className={"movieDesc"}>
                      {movie.overview.length > 300
                        ? movie.overview.substring(0, 300) + "..."
                        : movie.overview}
                    </p>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
