import React from "react";
import { Grid} from "semantic-ui-react";

import Sidebar from "./Sidebar";
import MovieGrid from "./MovieGrid";

export default class Gallery extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      genre: 28
    }
  }

  handleCatChange(value) {
    this.setState({genre: value});
    this.refs.child.fetchMovie(value);
  }

  render() {
    return (
      <div className={"pageCont"}>
        <div className={"sidebarCont"}>
          <Sidebar></Sidebar>
        </div>
        <div className={"mainCont"}>
          <div className={"container"}>
            <div className={"headerCont"}>
              <button
                onClick={() => this.handleCatChange(28)}
                className={this.state.genre === 28 ? "ui button active" : "ui button"}>
                Action
              </button>
              <button
                onClick={() => this.handleCatChange(35)}
                className={this.state.genre === 35 ? "ui button active" : "ui button"}>
                Comedy
              </button>
              <button
                onClick={() => this.handleCatChange(80)}
                className={this.state.genre === 80 ? "ui button active" : "ui button"}>
                Crime
              </button>
              <button
                onClick={() => this.handleCatChange(99)}
                className={this.state.genre === 99 ? "ui button active" : "ui button"}>
                Documentary
              </button>
              <button
                onClick={() => this.handleCatChange(14)}
                className={this.state.genre === 14 ? "ui button active" : "ui button"}>
                Fantasy
              </button>
              <button
                onClick={() => this.handleCatChange(9648)}
                className={this.state.genre === 9648 ? "ui button active" : "ui button"}>
                Mystery
              </button>
              <button
                onClick={() => this.handleCatChange(10749)}
                className={this.state.genre === 10749 ? "ui button active" : "ui button"}>
                Romance
              </button>
              <button
                onClick={() => this.handleCatChange(53)}
                className={this.state.genre === 53 ? "ui button active" : "ui button"}>
                Thriller
              </button>
            </div>
            <Grid columns={5} className={"gridCont"}>
              <MovieGrid ref="child" genre={this.state.genre}></MovieGrid>
            </Grid>
          </div>
        </div>
      </div>
    );
  }
}
