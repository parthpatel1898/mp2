import React from "react";
import { NavLink } from "react-router-dom";
import { faHome, faImages, faFilm } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from 'prop-types';

import "./Style.scss";

export default class Sidebar extends React.Component {
  render () {
    return (
      <div className={"navCont"}>
        <NavLink exact to={"/"} >
          <FontAwesomeIcon icon={faHome} />
        </NavLink>
        <NavLink to={"/gallery"}>
          <FontAwesomeIcon icon={faImages} />
        </NavLink>
        {this.props.showDetail ?
          <NavLink to={"/detail"} onClick={ (event) => event.preventDefault() }>
            <FontAwesomeIcon icon={faFilm} />
          </NavLink> : null
        }
      </div>
    );
  }
}

Sidebar.propTypes = {
  showDetail: PropTypes.bool
};
