import React from "react";
import { Input, Select } from "semantic-ui-react";

import Sidebar from "./Sidebar";
import MovieList from "./MovieList";

const options = [
  { key: "vote_count", text: "Vote Count", value: "vote_count" },
  { key: "vote_average", text: "Rating", value: "vote_average" }
];

const sortOrder = [
  { key: "asc", text: "Ascending", value: "asc" },
  { key: "desc", text: "Descending", value: "desc" }
];

export default class Home extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      category: "vote_count",
      query: "",
      sort_by: "desc"
    }
  }

  handleInputChange = e => {
    let query_local = e.target.value;
    this.setState({query : query_local});
    if (query_local.length === 0){
      this.refs.child.fetchMovie('a', this.state.category, this.state.sort_by);
    }
    else {
      this.refs.child.fetchMovie(query_local, this.state.category, this.state.sort_by);
    }
  }

  handleCatChange = (e, {value}) => {
    let cat_local = value;
    this.setState({category: cat_local});
    if (this.state.query.length === 0){
      this.refs.child.fetchMovie('a', cat_local, this.state.sort_by);
    }
    else {
      this.refs.child.fetchMovie(this.state.query, cat_local, this.state.sort_by);
    }
  }

  handleSortChange = (e, {value}) => {
    let sort_local = value;
    this.setState({sort_by: sort_local});
    if (this.state.query.length === 0){
      this.refs.child.fetchMovie('a', this.state.category, sort_local);
    }
    else {
      this.refs.child.fetchMovie(this.state.query, this.state.category, sort_local);
    }
  }

  render() {
    return (
      <div className={"pageCont"}>
        <div className={"sidebarCont"}>
          <Sidebar></Sidebar>
        </div>
        <div className={"mainCont"}>
          <div className={"container"}>
            <div className={"searchCont"}>
              <Input type='text' placeholder='Search...' onChange={this.handleInputChange} action className={"inputCont"}>
                <input />
                <Select compact options={options} defaultValue='vote_count' onChange={this.handleCatChange} />
                <Select compact options={sortOrder} defaultValue='desc' onChange={this.handleSortChange} />
              </Input>
            </div>
            <div className={"listCont"}>
              <MovieList ref="child"></MovieList>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
