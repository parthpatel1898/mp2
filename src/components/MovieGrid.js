import React from "react";
import { Grid, Image } from "semantic-ui-react";
import axios from 'axios';
import { Redirect } from 'react-router';

export default class MovieGrid extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      movies: [],
      open: false,
      selMovie: -1,
      redirect: false
    }
  }
  componentDidMount() {
    this.fetchMovie(28)
  }

  fetchMovie(genre) {
    let base_link = "https://api.themoviedb.org/3/discover/movie";
    let api_key = "1f101fb2fa544e8ca58a33576f7f3ce0";
    let language = "en-US";
    let page = "1";
    let include_adult = "false";
    let sort_by = "popularity.desc";
    let primary_year = "2019"
    let final_link = base_link + "?api_key=" + api_key + "&language=" + language + "&page=" + 
                     page + "&include_adult=" + include_adult + "&sort_by=" + sort_by + 
                     "&primary_release_year=" + primary_year + "&with_genres=" + genre;

    axios.get(final_link).then(res => {
      const movie_data = res.data;
      this.setState({ movies: movie_data.results });
    });
  }

  handleOpen(value) {
    this.setState({selMovie: value});
    this.setState({redirect: true});
  }

  render() {
    return (
      <Grid.Row>
        {this.state.redirect ? <Redirect push to={"/detail/" + this.state.selMovie} /> : <Redirect push to="/gallery" />}
        {this.state.movies.map(movie => (
          <Grid.Column
            className={"cardCont"}
            onClick={() => this.handleOpen(movie.id)}
            key={movie.id}>
            <Image
              alt={movie.title ? movie.title : movie.original_name}
              src={
                movie.poster_path
                  ? "http://image.tmdb.org/t/p/w342/" + movie.poster_path
                  : "https://thefilmuniverse.com/wp-content/uploads/2019/09/Poster_Not_Available2.jpg"
              }/>
            <p>{movie.title ? movie.title : movie.original_name}</p>
          </Grid.Column>
        ))}
      </Grid.Row>
    );
  }
}
