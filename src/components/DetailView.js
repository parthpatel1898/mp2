import React from "react";
import { Link } from 'react-router-dom'
import axios from 'axios';
import { Button, Grid, Image, Divider  } from "semantic-ui-react";

  
import Sidebar from "./Sidebar";

export default class DetailView extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      currMovie: {},
      currMovieId: -1
    }
    this.handleNext = this.handleNext.bind(this);
    this.handlePrevious = this.handlePrevious.bind(this);
  }

  componentDidMount() {
    let currId = this.props.match.params.id;
    this.setState({currMovieId: parseInt(currId)});
    this.fetchMovie(currId);
  }

  fetchMovie(movie_id) {
    let base_link = "https://api.themoviedb.org/3/movie/" + movie_id;
    let api_key = "1f101fb2fa544e8ca58a33576f7f3ce0";
    let language = "en-US";
    let final_link = base_link + "?api_key=" + api_key + "&language=" + language;

    axios.get(final_link)
    .then(res => {
      const movie_data = res.data;
      if (movie_data.adult) {

        let adultMovie = {
          title: "Movie Blocked",
          overview: "Information about this movie has been blocked because it contains adult contents.",
          poster_path: null,
          vote_average: null,
          vote_count: null,
          runtime: null,
          release_date: null
        }
        this.setState({ currMovie: adultMovie });
      }
      else {
        this.setState({ currMovie: movie_data });
      }
    })
    .catch(function (error) {

      let undefinedMovie = {
        title: "Movie not found",
        overview: "Please check the id of the movie to see if its correct and try again.",
        poster_path: null,
        vote_average: null,
        vote_count: null,
        runtime: null,
        release_date: null
      }

      this.setState({ currMovie: undefinedMovie });
      console.log(error);
    }.bind(this));
  }

  handleNext() {
    let currId = this.state.currMovieId + 1;
    this.setState({currMovieId: parseInt(currId)});
    this.fetchMovie(currId);
  }

  handlePrevious() {
    let currId = this.state.currMovieId - 1;
    this.setState({currMovieId: parseInt(currId)});
    this.fetchMovie(currId);
  }

  render() {
    return (
      <div className={"pageCont"}>
        <div className={"sidebarCont"}>
          <Sidebar showDetail={true}></Sidebar>
        </div>
        <div className={"mainCont"}>
          <div className={"container"}>
            <Grid columns={2} divided className={"detailView"}>
              {this.state.currMovie ? (
                <Grid.Row>
                  <Grid.Column width={5}>
                      <Image
                        alt={
                          this.state.currMovie.title
                            ? this.state.currMovie.title
                            : this.state.currMovie.original_name
                        }
                        src={
                          this.state.currMovie.poster_path
                            ? "http://image.tmdb.org/t/p/w342/" +
                              this.state.currMovie.poster_path
                            : "https://thefilmuniverse.com/wp-content/uploads/2019/09/Poster_Not_Available2.jpg"
                        }
                      />
                  </Grid.Column>
                  <Grid.Column width={11}>
                      <h1>{this.state.currMovie.title ? this.state.currMovie.title : this.state.currMovie.original_name}</h1>
                      <p>{this.state.currMovie.overview}</p>
                      <p>Rating: {this.state.currMovie.vote_average ? this.state.currMovie.vote_average + " / 10" : "-"}</p>
                      <p>Vote Count: {this.state.currMovie.vote_count ? this.state.currMovie.vote_count : "-"}</p>
                      <p>Runtime: {this.state.currMovie.runtime ? this.state.currMovie.runtime + " mins" : "-"}</p>
                      <p>Released On: {this.state.currMovie.release_date ? this.state.currMovie.release_date : "-"}</p>
                  </Grid.Column>
                </Grid.Row>
              ) : (
                "No Matching Movies"
              )}
            </Grid>
            <Divider />
            <div className={"btnCont"}>
              <Button as={Link} color={"red"}
                to={"/detail/" + (this.state.currMovieId - 1)}
                onClick={this.handlePrevious}> Previous
              </Button>
              <Button as={Link} color={"green"}
                to={"/detail/" + (this.state.currMovieId + 1)}
                onClick={this.handleNext}> Next
              </Button>
            </div>
            
          </div>
        </div>
      </div>
    );
  }
}
